import { stripIndents } from 'common-tags';

const parserSchemaName = 'x-parser-schema-id';

const InlineSchemaDescription = (props) => {
  if (props.schema.hasDescription()) {
    const formatted = stripIndents`
      * ${props.schema.description().replace('\n', '\n* ')}
    `;

    return `
      /**
      ${formatted}
      */
    `;
  }
  return '';
};
const InlineString = (props) => {
  const propertyType = props.schema.type();

  if (props.schema.enum()) {
    const enumValues = props.schema
      .enum()
      .map((x) => `'${x}'`)
      .join(' | ');
    return `${props.name}: ${enumValues}`;
  }

  return `${props.name}: ${propertyType}`;
};

const InlineNumber = (props) => {
  const propertyType = props.schema.type();
  if (props.schema.enum()) {
    const enumValues = props.schema.enum().join(' | ');
    return `${props.name}: ${enumValues}`;
  }
  return `${props.name}: ${propertyType}`;
};

const InlineExtension = (props) => {
  return `${props.name}: ${props.schema.extension(parserSchemaName)}`;
};

const InlineByType = (props) => {
  const [name, schema] = props.property;
  switch (schema.type()) {
    case 'string':
      return <InlineString name={name} schema={schema} />;
    case 'number':
      return <InlineNumber name={name} schema={schema} />;
    case 'object':
      return <InlineExtension name={name} schema={schema} />;
    default:
      return null;
  }
};

const InlinePropertyTypes = (props) =>
  Object.entries(props.properties).map(([key, value]) => [
    <InlineSchemaDescription schema={value} />,
    <InlineByType property={[key, value]} />,
    ';',
  ]);

export default InlinePropertyTypes;
