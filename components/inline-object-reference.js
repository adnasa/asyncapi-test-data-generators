import { Text } from '@asyncapi/generator-react-sdk';

const parserSchemaName = 'x-parser-schema-id';

const InlineObjectReference = (props) => {
  const mappedValues = Object.entries(props.properties)
    .filter(([_key, value]) => value.type() === 'object')
    .map(([_key, value]) => value.extension(parserSchemaName))
    .map(props.mapper);

  return <Text>{mappedValues.join('')}</Text>;
};

export default InlineObjectReference;
