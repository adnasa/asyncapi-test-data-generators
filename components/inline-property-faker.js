// eslint-disable-next-line no-unused-vars
import { Schema } from '@asyncapi/parser';
import { oneLine } from 'common-tags';
import { camel } from 'radash';

const parserSchemaName = 'x-parser-schema-id';

const InlineString = (props) => {
  if (props.schema.enum()) {
    const enumValues = props.schema
      .enum()
      .map((x) => `'${x}'`)
      .join(',');
    return `${props.name}: faker.helpers.arrayElement([${enumValues}])`;
  }

  // prioritize the example.
  if (props.schema._json.example) {
    return `${props.name}: '${props.schema._json.example}'`;
  }

  if (props.schema.format() === 'uuid') {
    return `${props.name}: faker.datatype.uuid()`;
  }

  if (props.schema.format() === 'date-time') {
    return `${props.name}: faker.date.past().toISOString()`;
  }

  return `${props.name}: faker.word.noun()`;
};

const InlineNumber = (props) => {
  if (props.schema.enum()) {
    const enumValues = props.schema.enum().join(',');
    return `${props.name}: faker.helpers.arrayElement([${enumValues}])`;
  }

  return `${props.name}: faker.datatype.number()`;
};

const InlineExtension = (props) => oneLine`
  ${props.name}: 
  ${camel(`create test ${props.schema.extension(parserSchemaName)}`)}()
`;

const InlinePropertyByType = (props) => {
  const [name, schema] = props;
  if (schema.isCircular()) {
    throw new Error('Recursive schema is not supported');
  }

  switch (schema.type()) {
    case 'string':
      return <InlineString name={name} schema={schema} />;
    case 'number':
      return <InlineNumber name={name} schema={schema} />;
    case 'object':
      return <InlineExtension name={name} schema={schema} />;
    default:
      return null;
  }
};

const InlinePropertyFaker = (props) =>
  Object.entries(props.properties).map(([key, value]) => {
    const rendered = InlinePropertyByType([key, value]);

    return rendered !== null ? [rendered, ','] : [];
  });

export default InlinePropertyFaker;
