import { Text } from '@asyncapi/generator-react-sdk';

const WrapInlineProperties = (props) => {
  return <Text>{props.wrap(props.inline)}</Text>;
};

export default WrapInlineProperties;
