import { stripIndent } from 'common-tags';

const ImportFaker = () => stripIndent`
  import { faker } from '@faker-js/faker';
`;

export default ImportFaker;
