const SEPARATORS = {
  COMMA: ',',
  COLON: ';',
  TYPE_UNION: '|',
};

export { SEPARATORS };
