// eslint-disable-next-line no-unused-vars
import { Schema } from '@asyncapi/parser';
import { File, Text } from '@asyncapi/generator-react-sdk';
import { oneLine } from 'common-tags';
import { dash, camel } from 'radash';
import InlineObjectReference from '../../components/inline-object-reference';
import ImportFaker from '../../components/import-faker';
import WrapInlineProperties from '../../components/wrap-inline-properties';
import InlinePropertyFaker from '../../components/inline-property-faker';
import LineBreak from '../../components/line-break';

/**
 * @typedef SchemaRenderParams
 * @type object
 * @property {schema} Schema
 * @property {string} schemaName
 * @property {string} params
 */

/**
 * @param SchemaRenderParams schemaRenderParams
 */
const renderSchema = (schemaRenderParams) => {
  const properties = schemaRenderParams.schema.properties();

  const functionName = camel(oneLine`
    create test ${schemaRenderParams.schemaName}
  `);

  return (
    <File name={`${dash(schemaRenderParams.schemaName)}.ts`}>
      <>
        <ImportFaker />
        <Text>
          {oneLine`
            import type
            { ${schemaRenderParams.schemaName} }
            from '../schemas/${dash(schemaRenderParams.schemaName)}';
          `}
        </Text>
        <InlineObjectReference
          properties={properties}
          mapper={(value) => `
            import ${camel(`create test ${value}`)} from './${dash(value)}';
          `}
        />
        <WrapInlineProperties
          inline={<InlinePropertyFaker properties={properties} />}
          wrap={(rendered) => [
            `const ${functionName} = (custom: Partial<${schemaRenderParams.schemaName}> = {}): ${schemaRenderParams.schemaName} => ({`,
            rendered,
            '...custom,',
            '});',
            <LineBreak />,
            <LineBreak />,
            `export default ${functionName}`,
          ]}
        />
      </>
    </File>
  );
};

export default renderSchema;
