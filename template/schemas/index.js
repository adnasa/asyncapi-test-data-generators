// eslint-disable-next-line no-unused-vars
import { AsyncAPIDocument, Schema } from '@asyncapi/parser';
import { File, Text } from '@asyncapi/generator-react-sdk';
import { dash } from 'radash';
import LineBreak from '../../components/line-break';

/**
 * @typedef TemplateParameters
 * @type {object}
 * @property {boolean} generateTestClient - whether or not test client should be generated.
 * @property {boolean} promisifyReplyCallback - whether or not reply callbacks should be promisify.
 */

/**
 * @typedef RenderArgument
 * @type {object}
 * @property {AsyncAPIDocument} asyncapi received from the generator.
 * @property {TemplateParameters} params received from the generator.
 */

/**
 * Function to render file.
 *
 * @param {RenderArgument} param0 render arguments received from the generator.
 */
const schemaBarrelExporter = ({ asyncapi }) => {
  const extractedSchemas = Object.keys(asyncapi.components().schemas());

  return (
    <File name="index.ts">
      <>
        <Text>
          {extractedSchemas.map((schemaName) => [
            `import type { ${schemaName} } from './${dash(schemaName)}'`,
            <LineBreak />,
          ])}
        </Text>
        <LineBreak />
        <Text>{`export { ${extractedSchemas.join(',')} }`}</Text>
      </>
    </File>
  );
};

export default schemaBarrelExporter;
