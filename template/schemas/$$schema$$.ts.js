// eslint-disable-next-line no-unused-vars
import { Schema } from '@asyncapi/parser';
import { File } from '@asyncapi/generator-react-sdk';
import { stripIndents } from 'common-tags';
import InlineObjectReference from '../../components/inline-object-reference';
import { dash } from 'radash';
import WrapInlineProperties from '../../components/wrap-inline-properties';
import InlinePropertyTypes from '../../components/inline-property-types';
import LineBreak from '../../components/line-break';

/**
 * @typedef SchemaRenderParams
 * @type object
 * @property {schema} Schema
 * @property {string} schemaName
 * @property {string} params
 */

/**
 * @param SchemaRenderParams schemaRenderParams
 */
const renderSchema = (schemaRenderParams) => {
  const properties = schemaRenderParams.schema.properties();
  const mapper = (value) => stripIndents`
    import type { ${value} } from './${dash(value)}'
  `;

  return (
    <File name={`${dash(schemaRenderParams.schemaName)}.ts`}>
      <>
        <InlineObjectReference properties={properties} mapper={mapper} />
        <WrapInlineProperties
          inline={
            <InlinePropertyTypes
              properties={schemaRenderParams.schema.properties()}
            />
          }
          wrap={(rendered) => [
            <LineBreak />,
            `type ${schemaRenderParams.schemaName} = {`,
            <LineBreak />,
            rendered,
            <LineBreak />,
            '};',
            <LineBreak />,
            `export { ${schemaRenderParams.schemaName} }`,
          ]}
        />
      </>
    </File>
  );
};

export default renderSchema;
