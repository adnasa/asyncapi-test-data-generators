# asyncapi-test-data-generators

## 1.1.0

### Minor Changes

- 96b80a8: add inline-object-reference
- eabab60: add inline-property-faker
- f28d52c: switch lodash -> radash
- 11ad2c3: add ImportFaker
- d730f45: add property components

## 1.0.0

### Major Changes

- Render test data generators
- Render schema as typescript types
